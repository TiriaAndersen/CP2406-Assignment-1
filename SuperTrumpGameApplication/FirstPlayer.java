import javax.swing.*;
import java.util.ArrayList;
import java.util.Scanner;

// First player starts the round sets categories.

public class FirstPlayer extends CurrentPlayer {

    public String category;
    public Player player;
    public ArrayList<Card> hand;
    public String name;

    public FirstPlayer(Player player){;
        super.getCategory();
        super.getTrump();
        this.player = player;
        this.hand = player.getHand();
        this.name = player.getName();
    }

    protected void chooseCategory() {

        Scanner input = new Scanner(System.in);
        int tempcategory;
        do {
            System.out.println("Choose a category: \n 1. Hardness \n 2. Specific gravity \n 3. Cleavage \n 4. Crustal abundance \n 5. Economic value \n Please enter the category number: ");
            tempcategory = input.nextInt();
        }while (tempcategory < 1 || tempcategory > 5);

        switch (tempcategory){
            case 1: category = "Hardness";
                break;
            case 2: category = "Specific gravity";
                break;
            case 3: category = "Cleavage";
                break;
            case 4: category = "Crustal abundance";
                break;
            case 5: category = "Economic value";
        }

    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public ArrayList<Card> getHand() {
        return hand;
    }

    @Override
    public String getCategory() {
        return category;
    }
}
