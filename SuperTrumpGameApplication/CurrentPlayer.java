import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.Scanner;

// Current player class to determine role of player, and implement the change in turns

public class CurrentPlayer extends Player {
    public Player player;
    public ArrayList<Card> hand;
    public Deck deck;
    public Card card;

    protected boolean isFirst = true;
    private boolean canChooseCategory = true;
    private boolean isNewRound;

    public static String category;
    public String trump;
    public int cardNum;
    public int playersNotPassed;
    protected int[] cardNumsInHand;


    public CurrentPlayer(){}

    public CurrentPlayer(Player player, int playersNotPassed,Deck deck){
        super();
        super.getHand();
        this.player = player;
        this.deck = deck;
        this.playersNotPassed = playersNotPassed;
        hand = player.getHand();

    }

    public void assignRole(){
        //System.out.println("AssignRole reached.");
        if (!player.checkIfEmptyHand())
            //System.out.println("Hand is not empty.");
            checkIfLastPlayerInRound();
            if (player.isCanPlay()) {
                //System.out.println(player.getName() + "Player can play.");
                System.out.println("CurrentPlayer is: " + player.getName());
                System.out.println("The current player's hand displayed: ");
                player.displayHand();

                if (isFirst) {
                    System.out.println("NEW ROUND HAS STARTED: All players who passed can play.");
                    FirstPlayer first = new FirstPlayer(player);
                    System.out.println( first.getName() + " is the first player");
                    playCard();

                    if (canChooseCategory){
                        first.chooseCategory();
                        category = first.getCategory();
                    }else if (category == "Specific Gravity" && cardNum == 46){
                        canChooseCategory = true;
                        assignRole();
                    }
                    setTrump();
                    isFirst = false;
                } else {
                    System.out.println(player.getName() + " is the player on turn.");
                    PlayerOnTurn playerturn = new PlayerOnTurn(player,playersNotPassed, deck, category, trump);
                    if (checkMustPass()){
                        System.out.println("You can't play any cards in your hand. You must pass.");
                        playerturn.pass();
                    } else {

                        if (playerturn.choosePlayOrPass()) {
                            playCard();
                        } else {
                            playerturn.pass();
                        }
                    }
                    System.out.println(playerturn.playersNotPassed + "PNP in current: " + playersNotPassed);
                    playersNotPassed = playerturn.playersNotPassed;
                    System.out.println(playerturn.playersNotPassed + "PNP in current: " + playersNotPassed);
                }
            }
    }

    protected void playCard() {
        chooseCard();
        card = new Card(cardNum);
        System.out.println(player.getName() + " played this card: ");
        card.displayCard();
        removeCardFromHand();
        // TODO: create discard pile system
        // If supertrump card is played:
        if (cardNum > 54) {
            if (cardNum != 60){
                category = card.getChangeCategory();
                canChooseCategory = false;
            }
            isFirst = true;
            assignRole();
        }
        //The trump not set when starting round (category must be selected first).
        if(!isFirst){
            setTrump();
        }

    }

    protected void chooseCard() {
        Scanner input = new Scanner(System.in);
        cardNumsInHand = new int[hand.size()];
        for (int i = 0; i < hand.size(); i++)
            cardNumsInHand[i] = hand.get(i).getCardNumber();
        System.out.println("Card numbers of cards in hand:" + Arrays.toString(cardNumsInHand));

        System.out.print("Choose a card to play from your hand. Please enter the card number: ");
        cardNum = input.nextInt();
        while (checkContains(cardNum) || validTrumpValue(cardNum)) {
            if(checkContains(cardNum)){
                System.out.print("Entered number invalid. Choose a card to play from your hand. Please enter the card number: ");
                cardNum = input.nextInt();
            }
            if(validTrumpValue(cardNum)) {

                System.out.print("The trump value is too low. Choose a card to play from your hand. Please enter the card number: ");
                cardNum = input.nextInt();
            }
        }
    }

    protected boolean validTrumpValue(int cardNum){
        if(cardNum < 55 && cardNum > 0 && !isFirst) {
            Card tempCard = new Card(cardNum);
            String tempTrump = tempCard.retrieveTrump(category);
            switch (category) {
                case "Hardness":
                    double tempHardness = Double.parseDouble(tempTrump);
                    double hardness = Double.parseDouble(trump);
                    if (tempHardness > hardness) {
                        return false;
                    }
                case "Specific gravity":
                    double tempSpecGrav = Double.parseDouble(tempTrump);
                    double specGrav = Double.parseDouble(trump);
                    if (tempSpecGrav > specGrav) {
                        return false;
                    }
                case "Cleavage":
                    String[] cleavage = {"poor/none", "1 poor", "2 poor", "1 good", "1 good,1 poor", "2 good", "3 good", "1 perfect", "1 perfect, 1 good", "1 perfect, 2 good", "2 perfect, 1 good", "3 perfect", "4 perfect", "6 perfect"};
                    ArrayList<String> cleavageList = new ArrayList<String>(Arrays.asList(cleavage));
                    int tempCleav = cleavageList.indexOf(tempTrump);
                    int cleav = cleavageList.indexOf(trump);
                    if (tempCleav > cleav)
                        return false;
                case "Crustal abundance":
                    String[] abundance = {"ultratrace", "trace", "low", "moderate", "high", "very high"};
                    ArrayList<String> abundanceList = new ArrayList<String>(Arrays.asList(abundance));
                    int tempAbund = abundanceList.indexOf(tempTrump);
                    int abund = abundanceList.indexOf(trump);
                    if (tempAbund > abund)
                        return false;
                case "Economic value":
                    String[] value = {"trivial", "low", "moderate", "high", "very high", "I'm rich!"};
                    ArrayList<String> valueList = new ArrayList<String>(Arrays.asList(value));
                    int tempVal = valueList.indexOf(tempTrump);
                    int val = valueList.indexOf(trump);
                    if (tempVal > val)
                        return false;
                default:
                    return true;
            }
        }
        else
            return false;
    }

    protected boolean checkContains(int cardNum) {
        for(int num : cardNumsInHand){
            if(num == cardNum)
                return false;
        }
        return true;
    }

    private boolean checkMustPass() {
        Iterator<Card> iterator = hand.iterator();
        boolean mustPass = true;
        while (iterator.hasNext()){
            card = iterator.next();
            if (card.getCardNumber() >= 55 && card.getCardNumber() <= 60){
                return false;
            } else {
                if (!validTrumpValue(card.getCardNumber())) {
                    mustPass = false;
                }
            }
        }
        return mustPass;
    }

    private void removeCardFromHand() {
        int index = 0 ;
        for(Card cardobj : hand){
            if(cardobj.getCardNumber() == card.getCardNumber()){
                index = hand.indexOf(cardobj);
            }
        }
        hand.remove(index);
        player.setHand(hand);
    }

    private void checkIfLastPlayerInRound() {
        System.out.println("check last player, players not passed:" + playersNotPassed);
        if(playersNotPassed == 1 && player.isCanPlay()){
            isFirst = true;
            canChooseCategory = true;
            isNewRound = true;
            System.out.println("All players except one has passed.");
        }
    }

    public String getCategory() {
        return category;
    }

    public void setTrump() {
        trump = card.retrieveTrump(category);
        //System.out.println("Trump value in CurrentPlayer: " + trump);
    }

    public String getTrump() {
        return trump;
    }

    public void setPlayer(Player player) {
        this.player = player;
        hand = player.getHand();

    }

    public Player getPlayer() {
        return player;
    }

    public void setNewRound(boolean newRound) {
        isNewRound = newRound;
    }

    public boolean isNewRound() {
        return isNewRound;
    }

    public void setFirst(boolean first) {
        isFirst = first;
    }

    public void setPlayersNotPassed(int playersNotPassed) {
        this.playersNotPassed = playersNotPassed;
    }
}

