import javax.swing.*;
import javax.swing.plaf.basic.BasicInternalFrameTitlePane;
import java.util.ArrayList;
import java.util.Scanner;
// Player class
public class Player {
    protected ArrayList<Card> hand;
    public String name;
    protected boolean canPlay = true;
    protected boolean AI = false;

    public Player(){

    }

    public void setPlayerName() {
        Scanner input = new Scanner(System.in);
        System.out.println("Please enter player name: ");
        name = input.nextLine();
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setAI(boolean AI) {
        this.AI = AI;
    }

    public String getName() {
        return name;
    }
    public void displayHand() {
        for(int i = 0; i<= (hand.size()-1); i++){
            hand.get(i).displayCard();
        }
    }

    public boolean isCanPlay() {
        System.out.println( name + "can play:"+ canPlay);
        return canPlay;
    }

    public void setHand(ArrayList<Card> hand) {
        this.hand = hand;
    }

    public ArrayList<Card> getHand() {
        return hand;
    }



    public void setCanPlay(boolean canPlay) {
        this.canPlay = canPlay;
    }

    public boolean checkIfEmptyHand() {
        if (hand.size() == 0) {
            return true;
        }
        else
            return false;
    }
}

