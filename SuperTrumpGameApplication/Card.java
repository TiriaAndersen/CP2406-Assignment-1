// Card class to categorise inside the card objects.
public class Card {
    private int cardNumber;
    private String name ;
    private String hardness;
    private String specificGravity;
    private String cleavage;
    private String abundance;
    private String value;
    private String changeCategory;
    ReadCardFile card = new ReadCardFile();

    public Card(int number){
        cardNumber = number;
        if(cardNumber <= 54){
            setName();
            setHardness();
            setSpecificGravity();
            setCleavage();
            setAbundance();
            setValue();
        }
        else{
            setSTName();
            setChangeCategory();
        }


    }

    public void displayCard(){

        System.out.println();
        System.out.println("Card Number: " + cardNumber);
        System.out.println("Name: " + name);
        if(cardNumber <= 54) {
            System.out.println("Hardness: " + hardness);
            System.out.println("Specific gravity: " + specificGravity);
            System.out.println("Cleavage: " + cleavage);
            System.out.println("Crustal abundance: " + abundance);
            System.out.println("Economic value: " + value);
        }
        else {
            System.out.println("Category changes to: " + changeCategory);
        }
        System.out.println();


    }

    public String retrieveTrump(String category) {
        switch (category) {
            case "Hardness":
                return hardness;
            case "Specific gravity":
                return specificGravity;
            case "Cleavage":
                return cleavage;
            case "Crustal abundance":
                return abundance;
            case "Economic value":
                return value;
        }
        return null;
    }



    private void setSTName() {
        int num = 701 + (cardNumber - 54 )*5;
        name = card.ReadCardFile(num);

    }

    public void setName() {
        int num = 13*(cardNumber) - 1;
        name = card.ReadCardFile(num);
    }

    private void setHardness() {
        int num = 13*(cardNumber - 1) + 8;
        String hardnessString = card.ReadCardFile(num);
        if(hardnessString.contains("-")){
            hardness = hardnessString.substring(hardnessString.indexOf("-") + 1);
        } else{
            hardness = hardnessString;
        }
    }

    private void setSpecificGravity() {
        int num = 13*(cardNumber-1) + 11;
        String specGravString = card.ReadCardFile(num);
        if(specGravString.contains("-")) {
            specificGravity = specGravString.substring(specGravString.indexOf("-") + 1);
        } else {
            specificGravity = specGravString;
        }
    }

    private void setCleavage() {
        int num = 13*(cardNumber-1) + 3;
        cleavage = card.ReadCardFile(num);
    }

    private void setAbundance() {
        int num = 13*(cardNumber - 1) + 4;
        abundance = card.ReadCardFile(num);
    }

    private void setValue() {
        int num = 13*(cardNumber - 1)  + 6;
        value = card.ReadCardFile(num);
    }

    private void setChangeCategory() {
        int num = 700 + (cardNumber - 54 )*5;
        changeCategory = card.ReadCardFile(num);

    }

    public int getCardNumber() {
        return cardNumber;
    }

    public String getChangeCategory() {
        return changeCategory;
    }





}

