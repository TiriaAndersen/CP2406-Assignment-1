import java.util.ArrayList;
import java.util.*;
import java.util.Random;

public class Deck {
    private static final int NUM_TOTAL_CARDS = 60;
    private ArrayList<Card> cards = new ArrayList<>(NUM_TOTAL_CARDS);

    public Deck() {
        for (int i = 1; i <= NUM_TOTAL_CARDS; i++) {
            Card card = new Card(i);
            cards.add(card);
        }
    }

    public ArrayList<Card> dealCards(int n) {
        ArrayList<Card> dealt = new ArrayList<>(n);
        for (int i = 0; i < n; i++) {
            dealt.add(cards.get(0));
            cards.remove(0);
        }
        return dealt;
    }

    public void randomiseCards() {
        Collections.shuffle(cards);
    }
}



