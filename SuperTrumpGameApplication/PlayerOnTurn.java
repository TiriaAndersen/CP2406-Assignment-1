import java.util.ArrayList;
import java.util.Iterator;
import java.util.Scanner;

// Player on turn views hand and can choose to pass or play a card.

public class PlayerOnTurn extends CurrentPlayer {
    public ArrayList<Card> hand;
    public String category;
    public String trump;
    //public int playersNotPassed;

    public PlayerOnTurn(Player player, int playersNotPassed,Deck deck,String category,String trump){
        super(player,playersNotPassed,deck);
        super.getCategory();
        super.getTrump();
        this.category = category;
        this.trump = trump;
        this.hand = player.getHand();
        this.isFirst = false;
    }

    protected boolean choosePlayOrPass() {
        Scanner input = new Scanner(System.in);
        int choice;
        do {
            System.out.println("Choose an action: \n 1. Play a card \n 2. Pass \n Please enter the option number:");
            choice = input.nextInt();
            // TODO: check if input is valid
        } while (choice < 1 || choice > 2);
        switch (choice){
            case 1: //playCard();
                return true;
            case 2: //pass();
                return false;
        }
        return true;
    }


    protected void pass() {
        System.out.println(player.getName() + " has passed.");
        pickUpCard();
        System.out.println("Players not passed in Player on Turn before: " + playersNotPassed);
        setPlayersNotPassed(playersNotPassed - 1);
        System.out.println("Players not passed in Player on Turn after: " + playersNotPassed);
        player.setCanPlay(false);
    }

    private void pickUpCard() {
        ArrayList<Card> newCard = deck.dealCards(1);
        hand.addAll(newCard);
        player.setHand(hand);
        System.out.println("Card picked up: ");
        newCard.get(0).displayCard();
    }

    @Override
    public String getCategory() {
        return category;
    }

}
