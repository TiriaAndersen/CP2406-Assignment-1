import java.util.Scanner;

// main application
public class Application {
    public static void main(String[] args){
        displayGameMenu();
    }

    public static void displayGameMenu() {
        Scanner input = new Scanner(System.in);
        int optionNum = 1;
        while (optionNum != 3){
            // TODO: check if valid
            System.out.println(" Choose an option: \n 1. Play New Game \n 2. Show Instructions \n 3. Quit Application \n Please enter option number: ");
            optionNum = input.nextInt();
            switch(optionNum) {
                case 1:
                    Game game = new Game();
                        break;
                case 2:
                    showInstructions();
                        break;
                case 3:
                    quitApplication();
                        break;
            }
        }


    }

    private static void quitApplication() {
        System.out.println("quit");


    }

    private static void showInstructions() {
        System.out.println("Intructions:");
        System.out.println("How to win: lose all your cards.");
        System.out.println("How to play: Players are dealt 8 cards each.");
        System.out.println("The first player will play a card a pick a category.");
        System.out.println("Each succeeding person can either play a card or pass.");
        System.out.println("If they play a card, the value of the card in the chosen category must be higher than the previous cars's. ");
        System.out.println("If they pass, they must pick up a card, and can't play till the next round.");
        System.out.println("Super Trump Cards are wildcards that change the category and start and new round.");
        System.out.println("The game continues until all but one player has won. The last player is the loser.");
        System.out.println("That's basically it. Good luck, don't lose!");

    }

}
