import java.util.ArrayList;
import java.util.Iterator;
import java.util.Scanner;

//Game class
public class Game {
    private static final int NUM_CARDS_IN_HAND = 3;

    public int numPlayers;
    public ArrayList<Player> players;
    public Deck deck;
    private CurrentPlayer current;
    private ArrayList<String> winners = new ArrayList<>();
    public int playersNotPassed;
    public String category;
    public String trump;

    public Game(){
        System.out.println("In game PNP before: " + playersNotPassed);
        createPlayers();
        shuffleDeck();
        dealCardsToPlayers();
        System.out.println("In game NUMPlayers: " + numPlayers);
        setPlayersNotPassed(numPlayers);
        System.out.println("In game PNP: " + playersNotPassed);
        assignCurrentPlayer(numPlayers);

        current.assignRole();
        displayCategory();
        displayTrump();
        // TODO: display trump min/max
        checkIfNewRound();
        turns();
        displayGameEnd();
    }

    private void createPlayers() {
        Scanner input = new Scanner(System.in);
        int x;

        do {
            System.out.println("Please enter a number of players between 3 - 5 players:");
            numPlayers = input.nextInt();
            // TODO: error for non-integer input
        } while (numPlayers < 1 || numPlayers > 5);
        System.out.println("Numplayers: " + numPlayers + "PNP: "+ playersNotPassed);

        players = new ArrayList<Player>(numPlayers);
        for(x = 0; x<=numPlayers - 1; x++){
            players.add(new Player());
            players.get(x).setPlayerName();;
        }
        if (numPlayers < 3){
            int numAI = 3 - numPlayers;
            for(x = 0; x<=numAI - 1; x++){
                players.add(new Player());
                String AIname = "Player " + (x+1);
                players.get(x).setName(AIname);
                players.get(x).setAI(true);
            }
        }
        // TODO: determine number of A.Is
    }

    private void shuffleDeck() {
        deck = new Deck();
        deck.randomiseCards();
    }

    private void dealCardsToPlayers() {
        for(Player player : players){
            ArrayList<Card> hand = deck.dealCards(NUM_CARDS_IN_HAND);
            player.setHand(hand);
        }

    }

    private void assignCurrentPlayer(int playersNotPassed) {
        Player currentPlayer = players.get(players.size()-1);
        current = new CurrentPlayer(currentPlayer,playersNotPassed,deck);
    }

    private void turns() {
        boolean run = true;
        while (run) {
            Iterator<Player> iterator = players.iterator();
            while (iterator.hasNext()) {
                if (players.size() == 1) {
                    run = false;
                    break;
                }
                current.setPlayer(iterator.next());
                current.assignRole();
                displayCategory();
                displayTrump();
                checkIfNewRound();
                if (checkIfWinner()) {
                    iterator.remove();
                    numPlayers = numPlayers - 1;
                }
                //System.out.println(players.size());
                //System.out.println("Size of winner list: " + winners.size());
            }
        }
        System.out.println(players.get(0).getName() + " has lost. Awww.");
    }

    private void checkIfNewRound() {

        if(current.isNewRound()){
            System.out.println("new round is " + current.isNewRound() + current.getPlayer().getName() + " they can play "+ current.isCanPlay());
            for(Player player : players){
                player.setCanPlay(true);
            }
            current.setNewRound(false);
            current.setPlayersNotPassed(numPlayers);
        }
    }

    private void displayCategory() {
        category = current.getCategory();
        System.out.println("The current category is: " + category);
    }

    private void displayTrump() {

        trump = current.getTrump();
        System.out.println("The current trump is: " + trump);


    }

    private void displayWinnersAndLosers() {
        System.out.println("Winners in order: ");
        for (int i = 1; i <= winners.size(); i++ ) {
            System.out.println(i + ". " + winners.get(i-1));
        }
        Player losingPlayer = players.get(0);
        System.out.println("Loser: \n" + losingPlayer.getName());
    }

    private boolean checkIfWinner() {
        boolean isWinner;
        isWinner = current.getPlayer().checkIfEmptyHand();
        if(isWinner){
            Player winningPlayer = current.getPlayer();
            winners.add(winningPlayer.getName());
            System.out.println(winningPlayer.getName() + " has won the game! Congratulations!");
        }
        return isWinner;
    }

    private void displayGameEnd() {
        System.out.println("\n The game has ended.");
        displayWinnersAndLosers();
    }

    public void setPlayersNotPassed(int playersNotPassed) {
        this.playersNotPassed = playersNotPassed;
    }
}

